const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const helmet = require("helmet");
const http = require("http");

mongoose.connect(
	"mongodb+srv://usuario:senha@banco-lb4zs.mongodb.net/banco?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useFindAndModify: false,
	}
);

const app = express();
app.use(cors());
app.use(helmet());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const server = http.createServer(app);

require("./models/User");
require("./models/Bill");
require("./models/ItemsBill");

app.use("/api", require("./routes"));
module.exports = server;
