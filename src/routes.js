const express = require("express");
const routes = express.Router();
const jwt = require("jsonwebtoken");

const checkJWT = (req, res, next) => {
	var token = req.headers["x-access-token"];

	if (!token) {
		return res.status(401).send({ auth: false, message: "No token provided" });
	}

	jwt.verify(token, "planner-backend", (error, decoded) => {
		if (error) {
			if (error.message.indexOf("jwt expired") > -1) {
				return res
					.status(401)
					.send({ auth: false, message: "No token provided" });
			} else {
				return res
					.status(500)
					.send({ auth: false, message: "Failed to authenticate token" });
			}
		}

		req.user_id = decoded._id;
		next();
	});
};

const UserController = require("./controllers/UserController");
routes.get("/users", checkJWT, UserController.getAll);
routes.get("/users/:id", checkJWT, UserController.getByID);
routes.post("/users", checkJWT, UserController.incluir);
routes.put("/users/:id", checkJWT, UserController.editar);
routes.delete("/users/:id", checkJWT, UserController.excluir);

const BillController = require("./controllers/BillController");
routes.get("/bills", checkJWT, BillController.getAll);
routes.get("/bills/:id", checkJWT, BillController.getByID);
routes.post("/bills", checkJWT, BillController.incluir);
routes.put("/bills/:id", checkJWT, BillController.editar);
routes.delete("/bills/:id", checkJWT, BillController.excluir);
routes.get("/bills/user_id/:user_id", checkJWT, BillController.getByUserID);

const ItemsBillController = require("./controllers/ItemsBillController");
routes.get("/itemsbills", checkJWT, ItemsBillController.getAll);
routes.get("/itemsbills/:id", checkJWT, ItemsBillController.getByID);
routes.post("/itemsbills", checkJWT, ItemsBillController.incluir);
routes.put("/itemsbills/:id", checkJWT, ItemsBillController.editar);
routes.delete("/itemsbills/:id", checkJWT, ItemsBillController.excluir);
routes.get(
	"/itemsbills/bill_id/:bill_id",
	checkJWT,
	ItemsBillController.getByBillID
);

// Others
routes.post("/google/account", UserController.googleAccount);
routes.post("/login", UserController.login);
routes.get("/logout", UserController.logout);

module.exports = routes;
