const mongoose = require("mongoose");
const ItemsBill = mongoose.model("ItemsBill");

module.exports = {
	async getAll(req, res) {
		const registros = await ItemsBill.find({})
			.sort("-createdAt")
			.populate("bill_id");
		return res.send(registros);
	},

	async getByID(req, res) {
		const registro = await ItemsBill.findById(req.params.id).populate(
			"bill_id"
		);
		return res.send(registro);
	},

	async incluir(req, res) {
		const registro = await ItemsBill.create(req.body);
		return res.send(registro);
	},

	async editar(req, res) {
		const registro = await ItemsBill.findByIdAndUpdate(
			req.params.id,
			req.body,
			{
				new: true,
			}
		);
		return res.send(registro);
	},

	async excluir(req, res) {
		await ItemsBill.findByIdAndRemove(req.params.id);
		return res.send();
	},

	async getByBillID(req, res) {
		const registros = await ItemsBill.find({
			bill_id: req.params.bill_id,
		}).sort("-createdAt");
		return res.send(registros);
	},
};
