const mongoose = require("mongoose");
const Bill = mongoose.model("Bill");

module.exports = {
	async getAll(req, res) {
		const registros = await Bill.find({})
			.sort("-createdAt")
			.populate("user_id");
		return res.send(registros);
	},

	async getByID(req, res) {
		const registro = await Bill.findById(req.params.id).populate("user_id");
		return res.send(registro);
	},

	async incluir(req, res) {
		const registro = await Bill.create(req.body);
		return res.send(registro);
	},

	async editar(req, res) {
		const registro = await Bill.findByIdAndUpdate(req.params.id, req.body, {
			new: true,
		});
		return res.send(registro);
	},

	async excluir(req, res) {
		await Bill.findByIdAndRemove(req.params.id);
		return res.send();
	},

	async getByUserID(req, res) {
		const registros = await Bill.find({ user_id: req.params.user_id }).sort(
			"-createdAt"
		);
		return res.send(registros);
	},
};
