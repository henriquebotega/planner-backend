const mongoose = require("mongoose");
const User = mongoose.model("User");

var jwt = require("jsonwebtoken");

module.exports = {
	async getAll(req, res) {
		const registros = await User.find({});
		return res.send(registros);
	},

	async getByID(req, res) {
		const registro = await User.findById(req.params.id);
		return res.send(registro);
	},

	async incluir(req, res) {
		const registro = await User.create(req.body);
		return res.send(registro);
	},

	async editar(req, res) {
		const registro = await User.findByIdAndUpdate(req.params.id, req.body, {
			new: true,
		});
		return res.send(registro);
	},

	async excluir(req, res) {
		await User.findByIdAndRemove(req.params.id);
		return res.send();
	},

	async login(req, res) {
		const item = await User.findOne({
			email: req.body.email,
			password: req.body.password,
		});

		if (item) {
			delete item["password"];

			var token = jwt.sign({ item }, "planner-backend", {
				expiresIn: "90m", // 1h30
			});

			return res.status(200).send({ auth: true, token: token });
		}

		return res.status(500).send({ error: "E-mail/Senha inválido!" });
	},

	async logout(req, res) {
		res.status(200).send({ auth: false, token: null });
	},

	async googleAccount(req, res) {
		const { googleId, name, email, access_token } = req.body.profile;

		const item = await User.findOne({ email });

		if (!item) {
			const newProfile = {
				name,
				email,
				access_token,
				password: googleId,
			};

			const registro = await User.create(newProfile);
			return res.send(registro);
		} else {
			const registro = await User.findByIdAndUpdate(
				item._id,
				{ access_token },
				{ new: true }
			);
			return res.send(registro);
		}
	},
};
