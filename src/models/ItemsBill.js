const mongoose = require("mongoose");

const ItemsBillSchema = new mongoose.Schema(
	{
		date: { type: String, required: true },
		title: { type: String, required: true },
		value: { type: Number, required: true },
		bill_id: { type: mongoose.Schema.Types.ObjectId, ref: "Bill" },
	},
	{ timestamps: true }
);

module.exports = mongoose.model("ItemsBill", ItemsBillSchema);
