const mongoose = require("mongoose");

const BillSchema = new mongoose.Schema(
	{
		type: { type: String, required: true },
		multiple: { type: Boolean, required: true, default: false },
		title: { type: String, required: true },
		value: { type: Number, default: 0 },
		user_id: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
	},
	{ timestamps: true }
);

module.exports = mongoose.model("Bill", BillSchema);
