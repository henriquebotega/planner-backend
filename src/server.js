const server = require("./app");

server.listen(process.env.PORT || 3333, () => {
	console.log("Backend planner is running on port 3333");
});
